[<p align="left">Previous: Shared Google Calendar Integration</p>](./4_QuickStart_Google_Calendar.md)

# 4. SendGrid Integration

[SendGrid](https://sendgrid.com/) is integrated with the Bicara infrastructure to support automated email notification at scale with [SendGrid Dynamic Templates](https://docs.sendgrid.com/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates).

The Pro Email API Plan is adopted for us to get access to [dedicated IP addresses](https://docs.sendgrid.com/ui/account-and-settings/dedicated-ip-addresses) in SendGrid to avoid emails from SendGrid being sent from groups of shared IP addresses and we will have control over the monitoring and maintenance for our sender's IP reputation.

## 4.1 Domain Authentication

On the SendGrid console, go to **Settings** and **Sender Authentication**. Under the **Domain Authentication** section (recommended by SendGrid to achieve the best deliverability), choose **Authenticate Your Domain** to use your domain as senders for SendGrid emails:

<img src="./imgs/sendgrid/domain_authentication.png" width="650" alt="SendGrid Domain Authentication" />

## 4.2 Dynamic Templates and Handlebars

Bicara uses [SendGrid Dynamic Templates](https://docs.sendgrid.com/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates) to allow sending custom email notifications to Bicara coaches and parents.

<img src="./imgs/sendgrid/dynamic_templates.png" width="650" alt="SendGrid Dynamic Templates" />

SendGrid Dynamic Templates support the [Handlebars](https://docs.sendgrid.com/for-developers/sending-email/using-handlebars) templating language to render the personalizations you send via the SendGrid API. As the following screenshot shows, all the parameters enclosed by `{{ }}` can be customized when sending the email:

<img src="./imgs/sendgrid/email_templates_with_handlebar.png" width="650" alt="SendGrid Dynamic Templates with Handlebar" />

Under the Bicara core functions folder, [`/utils/sendEmail.js`](../functions/utils/sendEmail.js) handles the SendGrid email sending which accepts the following parameters:

- @param email Target email address
- @param dynamicId SendGrid dynamic template id
- @param params SendGrid handlerbar parameters (e.g. sessionId, coachEmail, curationUrl)
- @param attachments An array of email attachments (optional)

The dynamic template IDs along with the SendGrid API Key will be used in [Step 6.2](./7_QuickStart_Functions.md) for configuring the environment variables for the Bicara Firebase Functions.

[<p align="right">Next: Deploy the Bicara Web Client</p>](./6_QuickStart_Web_Client.md)
