[<p align="left">Previous: Deploy Bicara Web Client</p>](./6_QuickStart_Web_Client.md)

# 6. Deploy Firebase Functions and Security Rules

Firebase Functions act as the serverless backend for the Bicara infrastructure. These functions utilise external APIs, storing information in the project's Firebase Realtime Database (RTDB), which is encrypted at rest.

## 6.1 Clone the Source Code

Clone the repository:

```
git clone https://gitlab.com/action-lab-aus/bicara/zoomsense-tops
```

Install dependencies (npm version <= 14):

```
cd functions

npm install
```

## 6.2 Configure Environment Variables for Firebase Functions

Run `firebase use --add` to define the project alias to be used for the Firebase Function (set the alias to be _default_). Find and choose the Firebase Project created in [Step 1](./2_QuickStart_Firebase.md).

Create a `.runtimeconfig.json` file under the root directory of the function folder. Copy the following JSON object into the file and update it with the credentials you received from the previous steps.

- `aws`: [Step 2.2 & Step 2.3](./3_QuickStart_Vonage.md)
- `google`: Credentials can be found under the Credentials sections in [GCP APIs and Services](https://console.cloud.google.com/apis/dashboard)
- `opentok`: [Step 2.1](./3_QuickStart_Vonage.md)
- `sendgrid`: [Step 4.2](./5_QuickStart_SendGrid.md)
- `calender`: [Step 3.3](./4_QuickStart_Google_Calendar.md)

```JSON
{
  "aws": {
    "cloudfront_url": "AWS_CLOUDFRONT_URL",
    "mediaconvert_role": "AWS_MEDIACONVERT_ROLE",
    "mediaconvert_queue": "AWS_MEDIACONVERT_ROLE",
    "mediaconvert_endpoint": "AWS_MEDIACONVERT_ENDPOINT",
    "region": "AWS_REGION",
    "bucket_name": "S3_BUCKET_NAME"
  },
  "google": {
    "client_id": "GOOGLE_OAUTH2.0_CLIENT_ID",
    "client_secret": "GOOGLE_OAUTH2.0_CLIENT_SECRET",
    "redirect_uri": "http://localhost:3000",
    "calendar_id": "SHARED_GOOGLE_CALENDAR_ID@group.calendar.google.com"
  },
  "opentok": {
    "api_secret": "VONAGE_PROJECT_SECRET",
    "api_key": "VONAGE_PROJECT_API_KEY"
  },
  "sendgrid": {
    "api_key": "SENDGRID_API_KEY",
    "sender": "SENDGRID_SENDER_EMAIL",
    "curation_ready": "DYNAMIC_TEMPLATE_ID",
    "totp_token": "DYNAMIC_TEMPLATE_ID",
    "curation_error": "DYNAMIC_TEMPLATE_ID",
    "curation_completed": "DYNAMIC_TEMPLATE_ID",
    "session_reminder": "DYNAMIC_TEMPLATE_ID"
  },
  "calender": {
    "address": "PUBLIC_CALENDAR_ADDRESS_IN_ICAL_FORMAT"
  },
  "firebase_credentials": {
    "type": "service_account",
    "project_id": "*******",
    "private_key_id": "*******",
    "private_key": "-----BEGIN PRIVATE KEY-----********\n-----END PRIVATE KEY-----\n",
    "client_email": "*******@appspot.gserviceaccount.com",
    "client_id": "*******",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://oauth2.googleapis.com/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/*******%40appspot.gserviceaccount.com"
  },
  "anonymous": {
    "encryption_secret": "ENCRYPTION_SECRET_ANY_STRING",
    "encryption_key": "ENCRYPTION_KEY_ANY_STRING"
  },
  "functions_url": "https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net/",
  "firebase_rtdb": "https://FIREBASE_DATABASE_URL.firebaseio.com/",
  "localhost_functions_url": "http://localhost:5001/FIREBASE_PROJECT_ID/us-central1/",
  "site_url": "https://FIREBASE_PROJECT_ID.web.app""
}
```

Set environment configuration for your project by using the `firebase functions:config:set` command in the Firebase CLI. More details can be found on [Environment Configuration](https://firebase.google.com/docs/functions/config-env).

For example, you can run the following command on Mac/Linux to set environment configuration from the `.runtimeconfig.json` file (please replace the `FIREBASE_PROJECT_ID` with your Firebase Project ID below):

```
firebase functions:config:set FIREBASE_PROJECT_ID="$(cat .runtimeconfig.json)"
```

## 6.3 Deploy Firebase Functions & Rules

The Firebase Function repository also contains Firebase Realtime Database rules for secured data access under [`database.rules.json`](../database.rules.json).

To deploy Firebase functions with RTDB rules:

```
cd functions

firebase deploy
```

[<p align="right">Next: Initialize the Bicara RTDB</p>](./8_QuickStart_Bicara.md)
