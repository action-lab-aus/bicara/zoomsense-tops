[<p align="left">Previous: Setting up a Firebase Project</p>](./2_QuickStart_Firebase.md)

# 2. Configure Vonage Video API & AWS Services

[Vonage Video API](https://www.vonage.com.au/log-in/?icmp=utilitynav_login_novalue) allows us to build a custom video experience within any mobile, web, or desktop application built on the WebRTC industry standard.

To configure the Vonage Video API, please use the Video API Login via the link above to log into your Vonage account.

<img src="./imgs/vonage/vonage_video_api.png" width="600" alt="Vonage Video API" />

The newly created account will be in the Trail Plan by default. The Trial Plan should be sufficient for development and testing purposes. However, we need to upgrade it to the Standard Plan to support the real deployment. More details regarding the pricing plan and cost can be found on [Vonage Video API Pricing](https://www.vonage.com.au/communications-apis/video/pricing/).

## 2.1 Create a Vonage Video API Project

To get started with using Vonage Video API, please click **Projects** and **Create New Project** on the Vonage Video API console. Choose **Create Custom Project** (which provides us with full control over functionality) as the following screenshot shows:

<img src="./imgs/vonage/vonage_create_new_project.png" width="600" alt="Vonage - Create New Project" />

The Project API Key and Secret can be found on the top of the Vonage Project page:

<img src="./imgs/vonage/vonage_api_key_and_secret.png" width="600" alt="Vonage - API Key and Secret" />

## 2.2 Configure Archiving Storage and Callback

Vonage Video API archiving lets us record, save and retrieve session recordings with easy integration with cloud storage services. On the Vonage Project page, scroll down to the **Archiving** section. Click **Edit** to configure the Cloud Storage connection. Since Bicara uses quite a few other AWS services to support video transcoding, transcription generation and event notifications, Amazon S3 Bucket is used for the Vonage archiving storage.

Under **Callback URL**, set the Configured URL to be _https://us-central1-PROJECT_ID.cloudfunctions.net/Recording-archiveOnUploaded_:

<img src="./imgs/vonage/vonage_archive_configuration.png" width="600" alt="Vonage - Archiving Configruation" />

The Bicara [Recording Plugin](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-plugin-recording/-/blob/main/functions/index.js) - `Recording-onActualStartTimeAdded` Firebase Function, checks whether the recording is enabled and put the `recordingStart` flag to the recording node. This will trigger a POST request to the [`opentokApp-opentokApp`](../functions/functions/opentokApp.js) Core Bicara Firebase Function (POST `/archive/start/:sessionId`) for starting the Vonage session archiving.

We have created an AWS CloudFront Distribution with S3 to securely deliver session videos and transcriptions with low latency. You can find more instructions here regarding how to [Create an AWS CloudFront Distribution](https://aws.amazon.com/blogs/networking-and-content-delivery/amazon-s3-amazon-cloudfront-a-match-made-in-the-cloud/).

## 2.3 AWS Elemental MediaConvert

[AWS Elemental MediaConvert](https://aws.amazon.com/mediaconvert/) is integrated with the Bicara backend for generating curated assets for a specific TOPS session.

The [Bicara Curation Plugin](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-plugin-curation/-/blob/main/functions/index.js) detects the curation asset creation event in the RTDB (`/data/curationAsset/{meetingId}/{assetId}`) and start creating jobs by providing the source recording URL, start and end timecode to the MediaConvert service.

### 2.3.1 MediaConvert IAM Role

To get started with setting up AWS Elemental MediaConvert for Bicara, create an IAM Role for MediaConvert with **AmazonS3FullAccess** & **AmazonAPIGatewayInvokeFullAccess** as the following screenshot shows:

<img src="./imgs/aws/media_convert_role.png" width="600" alt="MediaConvert Role" />

### 2.3.2 MediaConvert Queue

On the AWS console, search for MediaConvert, and click the **Queues** tab to create a new MediaConvert queue for the infrastructure. You will be able to find the Queue ARN (needed in [Step 6](./7_QuickStart_Functions.md)) on the queue details page:

<img src="./imgs/aws/media_convert_queue.png" width="600" alt="MediaConvert Queue" />

### 2.3.3 MediaConvert Endpoint

The MediaConvert endpoint (needed in [Step 6](./7_QuickStart_Functions.md)) can be found under the **Account** tab:

<img src="./imgs/aws/media_convert_endpoint.png" width="600" alt="MediaConvert Endpoint" />

## 2.4 Configure Amazon Simple Notification Service (SNS)

Bicara backend leverages [Amazon S3 Event Notification](https://docs.aws.amazon.com/AmazonS3/latest/userguide/NotificationHowTo.html) integration with [Amazon SNS](https://aws.amazon.com/sns/?whats-new-cards.sort-by=item.additionalFields.postDateTime&whats-new-cards.sort-order=desc) to notify Bicara plugins of the completion of transcription generation ([Transcribe Plugin](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-plugin-transcribe)) and MediaConvert jobs ([Curation Plugin](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-plugin-curation)) when the files get created in the dedicated S3 Bucket.

### 2.4.1 Create an SNS Topic

To configure the event notification channels for these services, create new SNS Topics on the AWS console:

<img src="./imgs/aws/sns_topics.png" width="600" alt="SNS Topics" />

Select the **Standard** type for the topic and give it a meaningful name. Under the **Access Policy** section, use the Advance JSON Editor to define the access rules for the new topic. Please use the JSON template below for setting up the access policy (by replacing the values for `SOURCE_OWNER`, `TOPIC_NAME`, and `BUCKET_NAME`).

<img src="./imgs/aws/create_sns_topic.png" width="600" alt="Create SNS Topics" />

```JSON
{
  "Version": "2008-10-17",
  "Id": "__default_policy_ID",
  "Statement": [
    {
      "Sid": "__default_statement_ID",
      "Effect": "Allow",
      "Principal": {
        "AWS": "*"
      },
      "Action": [
        "SNS:GetTopicAttributes",
        "SNS:SetTopicAttributes",
        "SNS:AddPermission",
        "SNS:RemovePermission",
        "SNS:DeleteTopic",
        "SNS:Subscribe",
        "SNS:ListSubscriptionsByTopic",
        "SNS:Publish"
      ],
      "Resource": "arn:aws:sns:ap-southeast-2:SOURCE_OWNER:TOPIC_NAME",
      "Condition": {
        "ArnLike": {
          "aws:SourceArn": "arn:aws:s3:*:*:BUCKET_NAME"
        }
      }
    }
  ]
}
```

### 2.4.2 Create a Subscription for the SNS Topic

Before creating a Subscription on the AWS console, please update the Transcribe & Curation Plugin functions first since SNS will need to send a confirmation request to the provided endpoint to verify the subscription.

Under the functions folder for both the Transcribe & Curation plugin, comment out the `transcriptionOnCompletedSns` & `curationOnCompletedSns` function in the source code and use the following code snippets instead for deployment.

```JS
// AWS SNS endpoint for subscription verficiation
const request = require('request');

exports.curationOnCompletedSns = functions.https.onRequest(async (req, res) => {
  let payload = JSON.parse(req.body);
  console.log('payload: ', payload);
  try {
    if (req.header('x-amz-sns-message-type') === 'SubscriptionConfirmation') {
      const url = payload.SubscribeURL;
      await request(url, handleSubscriptionResponse);
    } else if (req.header('x-amz-sns-message-type') === 'Notification') {
      console.log(payload);
    } else {
      throw new Error(`Invalid message type ${payload.Type}`);
    }
  } catch (err) {
    console.error(err);
    res.status(500).send('Oops');
  }
  res.send('Ok');
});

const handleSubscriptionResponse = function (error, response) {
  if (!error && response.statusCode === 200) {
    console.log('Yes! We have accepted the confirmation from AWS.');
  } else {
    throw new Error(`Unable to subscribe to given URL`);
  }
};
```

Please be aware that all the plugin functions are namespaced, the function being used for the SNS verification is actually `Transcribe-transcriptionOnCompletedSns` & `Curation-curationOnCompletedSns`. To make sure the correct, namespaced functions get deployed to Firebase, change the `index.js` file locally to be `entrypoint.js`, and create a new `index.js` file with the following code in it before the deployment:

```JS
exports.PLUGIN_NAME = require("./entrypoint");
```

On the Topic detail page, click **Create Subscription** to create a new subscription for the topic. Select **HTTPS** as the protocol, and select `https://us-central1-PROJECT_ID.cloudfunctions.net/Curation-curationOnCompletedSns`/`https://us-central1-PROJECT_ID.cloudfunctions.net/Transcribe-transcriptionOnCompletedSns` as the endpoint.

<img src="./imgs/aws/sns_subscription.png" width="600" alt="SNS Subscription" />

The confirmation request will be sent to the Firebase endpoint automatically, and after the successful verification, the subscription status will be changed to **Confirmed**.

Please revert `transcriptionOnCompletedSns` & `curationOnCompletedSns` back to their original state after confirming the subscription from SNS.

### 2.4.3 S3 Event Notifications

In the S3 Bucket page on the AWS console, click **Properties** and go to the **Event notifications** section. Click **Create event notification** to define the event notification rules and connections. As the following screenshot shows, use the Vonage API Key obtained in Step 2.1 as the notification Prefix, and define `curated.mp4` as the Suffix for the `CuratedAssetUploadEvent` notification, and `.json` as the Suffix for the `TranscriptionUploadEvent`. For **Object creation**, select **All object create events** for **Event types**:

<img src="./imgs/aws/s3_event_notification_1.png" width="600" alt="S3 Event Notification" />

Scrolling down to the **Destination** section, select **SNS topic** as the destination, and use the SNS topic ARN obtained from Step 2.4.1 for setting up the connection:

<img src="./imgs/aws/s3_event_notification_2.png" width="600" alt="S3 Event Notification" />

[<p align="right">Next: Shared Google Calendar Integration</p>](./4_QuickStart_Google_Calendar.md)
