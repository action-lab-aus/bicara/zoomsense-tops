# Quick Start Guide

Bicara is a distributed service architecture spanning multiple providers:

### Google Firebase

To deploy the full Bicara infrastructure on your own, you need to have a [Firebase Project](https://firebase.google.com/) with **Blaze Plan (Pay as you go)** to use the Firebase services including:

- Firebase Realtime Database
- Firebase Functions
- Firebase Storage
- Firebase Authentication
- Firebase Hosting

### Vonage Video API

[Vonage Video API](https://www.vonage.com.au/log-in/?icmp=utilitynav_login_novalue) allows us to build a custom video experience within any mobile, web, or desktop application built on the WebRTC industry standard.

### Google Calendar Integration

A shared Google Calendar is integrated with Bicara to handle the scheduling for Bicara sessions. This calendar will be shared among all coaches for them to create or update sessions and manage sharing for the calendar access.

### SendGrid

[SendGrid](https://sendgrid.com/) is integrated with the Bicara infrastructure to support automated email notification at scale with [SendGrid Dynamic Templates](https://docs.sendgrid.com/ui/sending-email/how-to-send-an-email-with-dynamic-transactional-templates). Action Lab uses a shared SendGrid account with [Pro Email API Plans](https://sendgrid.com/pricing/) to support email automation for a wide range of research projects.

The Pro Email API Plan is adopted for us to get access to [dedicated IP addresses](https://docs.sendgrid.com/ui/account-and-settings/dedicated-ip-addresses) in SendGrid to avoid emails from SendGrid being sent from groups of shared IP addresses and we will have control over the monitoring and maintenance for our sender's IP reputation.

### PiP+ Integration

Since all TOPS users will be using [Partners in Parenting Plus](https://partnersinparenting.com.au/)(PiP+) in parallel, the user accounts are associated loosely via the shared email and user ID in Firebase.

PiP+ will expose endpoints for Bicara to fetch parent-related data (PiP+ module activities responses, goal selection, etc.) for TOPS coaches to prepare for the session in advance.

[<p align="right">Next: Setting up a Firebase Project</p>](./2_QuickStart_Firebase.md)
