[<p align="left">Previous: Instructions</p>](./1_QuickStart_Instructions.md)

# 1. Create a Firebase Project

The Firebase project will be used as the serverless backend for handling data storage, real-time event triggers, hosting, and authentication. You can find more detailed instructions here on how to [Create a Firebase Project](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/2_QuickStart_Firebase.md), [Configure Firebase CLI](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/5_QuickStart_Firebase_CLI.md), and [Configure GCP IAM Permissions](https://gitlab.com/action-lab-aus/zoomsense/zoomsense/-/blob/master/docs/7_QuickStart_IAM.md).

[<p align="right">Next: Configure Vonage Video API & AWS Services</p>](./3_QuickStart_Vonage.md)
