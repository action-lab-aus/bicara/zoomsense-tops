[<p align="left">Previous: Deploy Firebase Functions</p>](./7_QuickStart_Functions.md)

# 7. Initialize the Bicara RTDB

## 7.1 Configure the Bicara Whitelist

Bicara web client is restricted to only a few TOPS coaches to access. In the Bicara infrastructure, we store a coach whitelist to make sure that only whitelisted coach accounts will be allowed to access the system.

The [`rtdbInit.js`](./rtdbInit.js) file under the `/docs` folder provides a tiny script for loading the initial whitelist into the Firebase RTDB with the Firebase Admin SDK (SDK credentials obtained via [Step 1](./2_QuickStart_Firebase.md)).

Whenever there is a new account created via **Google Authentication** on the Bicara coach dashboard, the[ `auth-checkWhitelistUser`](../functions/functions/auth.js) function in Bicara Core listens to the new user account creation event in Firebase Authentication.

- If the user email does not exist under the whitelist data node, the user account will be deleted and notification will be sent to the web client via the `verified: false` flag under the `db.ref("users").child(uid)` data node.
- Otherwise, the function will set the **Coach** custom claim for the new user account to allow coaches to access (check the [`database.rules.json`](../database.rules.json) for more information) the data needed for running and curating a Bicara session.
- On the web client, we will notify the user of the account creation and automatically log out the user and ask them to sign in again to access the coach dashboard (custom claim set needs re-authentication to take effect).

## 7.2. Create a Bicara Parent Account

Bicara parent accounts can be created either via the coach dashboard or the shared Google Calendar.

### 7.2.1 Create Parent Accounts via Coach Dashboard

<img src="./imgs/frontend/create_new_parent.png" width="400" alt="Create a New Parent" />

When adding parent accounts via the coach dashboard, the [`parentAuth-createParentProfile`](../functions/functions/parentAuth.js) Firebase onCall function will be called to create a new parent Firebase account. A random password will be assigned to the newly created Firebase account, but it will never be used for parents to authenticate into Bicara.

Instead, we provide two other different ways for handling parent authentication:

#### (Anonymous Authentication) Join the Bicara Session via a Google Calendar Invite

The Bicara scheduling system will handle unique meeting link generation and send notifications via Google Calendar when the meeting link is attached to the calendar invite. The embedded meeting link will direct the parent to the Bicara web client and perform an anonymous authentication workflow that allows users to join the session using an anonymous Firebase account.

#### (Custom Token Authentication) Access Learning Journey via a Time-based One-time Link

Parents can access the learning assets curated by their coach via a time-based one-time access link. When the link is provided on the Bicara web client, [`parentAuth-parentAuthenticate`](../functions/functions/parentAuth.js) will exchange the one-time access token encoded in the link for a Firebase Custom Token and allow users to sign in with the custom token returned (via `firebase.auth().signInWithCustomToken(token)`).

### 7.2.2 Create Parent Accounts via Shared Calendar Integration

If the coach decides to create the first session with the parent via the shared Google Calendar, the [`meeting-onMeetingAdded`](../functions/functions/meeting.js) function will check whether the parent record has been created in Firebase or not. If the account is not created yet, we will then create a parent account using the user ID retrieved from the calendar invite.
