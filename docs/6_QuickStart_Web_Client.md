[<p align="left">Previous: SendGrid Integration</p>](./5_QuickStart_SendGrid.md)

# 5. Deploy the Bicara Web Client

## 5.1 Clone the Source Code

Clone the repository:

```
git clone https://gitlab.com/action-lab-aus/bicara/zoomsense-tops
```

Install dependencies (npm version <= 14):

```
cd app

npm install
```

## 5.2 Create a Firebase Web App

Before deploying the Bicara Web Client to Firebase, we need to create a Firebase Web App to get started.

<img src="./imgs/firebase/create_web_app.png" width="650" alt="Create a Firebase Web App" />

Register the app by providing the App nickname. Choose **Also set up Firebase Hosting** for this app to get Firebase Hosting set up for you.

Choose **Continue** for all the remaining sections, and you will be directed back to the **Project settings** tab. Under **Your app**, you will see a Web App created successfully. Choose **Config** under the **SDK setup and configuration** section to get the Firebase configuration object containing keys and identifiers for your app.

## 5.3 Set up the Firebase Configurations

Create `.env.development` & `.env.production` files under `bicara-core/app` with the configuration details received in the previous step as:

```
VUE_APP_FB_KEY=firebaseConfig.apiKey
VUE_APP_FB_AUTH=firebaseConfig.authDomain
VUE_APP_RTDB=firebaseConfig.databaseURL
VUE_APP_FB_ID=firebaseConfig.projectId
VUE_APP_FB_BUCKET=firebaseConfig.storageBucket
VUE_APP_FB_FUNCTIONS=https://us-central1-FIREBASE_PROJECT_ID.cloudfunctions.net
VUE_APP_MESSAGING_SENDER_ID=firebaseConfig.messagingSenderId
VUE_APP_APP_ID=firebaseConfig.appId
VUE_APP_PLUGIN_DIRECTORY=GIT_REPOSITORY_FOR_PLUGIN_DIRECTORY
VUE_APP_VONAGE_API_KEY=VONAGE_API_KEY # Step 2.1
VUE_APP_S3_BUCKET=S3_BUCKET_NAME_FOR_VONAGE_ARCHIVING # Step 2.2
VUE_APP_CLOUDFRONT_URL=CLOUD_FRONT_URL_FOR_S3_BUEKT # Step 2.2
```

## 5.4 Build and Deploy the Web Client

Make sure under the `.firebaserc` file, the default project has been set to the Firebase Project you have created as:

```JSON
{
  "projects": {
    "default": "FIREBASE_PROJECT_ID"
  }
}
```

Build the Web Client and deploy it to Firebase Hosting:

```
npm run build

firebase deploy
```

[<p align="right">Next: Deploy Firebase Functions</p>](./7_QuickStart_Functions.md)
