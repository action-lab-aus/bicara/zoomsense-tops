// Load the `serviceAccount` via an admin SDK on your local machine
const serviceAccount = require("path/to/BICARA_ADMIN_SDK.json");
const admin = require("firebase-admin");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  // Update the `databaseURL` before runnning the script
  databaseURL: "https://BICARA_DEFAULT_RTDB.firebaseio.com",
});
const db = admin.database();

// Replace the emails with real coach emails
const topsWhitelist = ["COACH_EMAIL_1", "COACH_EMAIL_2"];
const meta = require("./meta.json");

(async () => {
  // Load whitelisted users
  const promises = [];
  topsWhitelist.forEach((user) => {
    promises.push(db.ref("topsWhitelist").push(user));
  });
  await Promise.all(promises);

  // Load TOPS session metadata
  await db.ref("meta").set(meta);
})();
