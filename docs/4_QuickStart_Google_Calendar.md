[<p align="left">Previous: Configure Vonage Video API & AWS Services</p>](./3_QuickStart_Vonage.md)

# 3. Shared Google Calendar Integration

A shared Google Calendar is created to handle the scheduling for Bicara sessions:

<img src="./imgs/calendar/shared_google_calendar.png" width="600" alt="Shared Google Calendar" />

## 3.1 Create a Shared Google Calendar

To create a new shared Google Calendar, click **Add other calendars** and select **Create new calendar**. Please set the calendar name, description and timezone for the calendar accordingly:

<img src="./imgs/calendar/calendar_setting.png" width="600" alt="Shared Calendar Setting" />

## 3.2 Configure the Access for the Shared Calendar

The Google Calendar will be shared among all coaches to access the session details and allows the Bicara backend to fetch events scheduled and create meeting records in the Bicara RTDB. Under the **Share with specific people** section, please add all TOPS coaches with the permission to **Make changes and manage sharing**:

<img src="./imgs/calendar/shared_with_coaches.png" width="600" alt="Shared with Coaches" />

## 3.3 Public Calendar Address (for Bicara Backend)

The public address in iCal format for the shared Google Calendar can be found under the **Integrate calendar** section. This public URL will be used by the Bicara core function [`scheduler-fetchCalendarEvents`](../functions/functions/scheduler.js) to fetch new calendar events using a CRON job in the backend.

<img src="./imgs/calendar/calendar_ical_address.png" width="600" alt="Calendar iCal Address" />

## 3.4 Enable Google Calendar API for the Project

Please visit the Google Calendar API page via _https://console.cloud.google.com/apis/api/calendar-json.googleapis.com/overview?project=PROJECTID_ to enable the Calendar API for the Google Project created in Step 1.

## 3.5 OAuth Credentials for Managing the Shared Calendar

Since the shared Google Calendar is private, we need to grant the Schedule Plugin with permissions to insert/update new calendar events using Google OAuth 2.0.

Please go to the [Google OAuth 2.0 Playground](https://developers.google.com/oauthplayground/) and click the settings button on the top right of the screen. Click **Use your own OAuth credentials** to set the OAuth Client ID and OAuth Client Secret for the OAuth Playground. OAuth credentials can be found under the Credentials sections in [GCP APIs and Services](https://console.developers.google.com).

<img src="./imgs/calendar/oauth_id_and_secret.png" width="600" alt="OAuth Playground - Use Your Own OAuth Credentials" />

As the screenshot above shows, under **Step 1 Select & authorize APIs**, enter _https://www.googleapis.com/auth/calendar_ and click **Authorize APIs**.

The OAuth 2.0 playground will ask you to choose an account for authorizing the access. Please make sure that you use a Google account that has the permission to **Make changes and manage sharing** for the shared calendar (added via Step 3.2) for authorization.

You will be prompted with a warning message indicating that the app is not verified. Please select **Show Advanced** and click **Go to project-PROJECT_ID (unsafe)** to continue with the authorization.

<img src="./imgs/calendar/app_verification.png" width="600" alt="OAuth Playground - App Verification" />

Click **Continue** to proceed with the authorization process:

<img src="./imgs/calendar/continue_authorization.png" width="600" alt="OAuth Playground - Continue Authorization" />

You will then be provided with the Authorization code. Please click **Exchange authorization code for tokens** to get the refresh token and access token for the app:

<img src="./imgs/calendar/authorization_code.png" width="600" alt="OAuth Playground - Authorization Code" />

The credentials will be provided on the bottom right section of the playground. Please copy the credentials into the Firebase RTDB as the following screenshot shows:

<img src="./imgs/calendar/credentials.png" width="600" alt="OAuth Playground - Credentials" />

<img src="./imgs/calendar/rtdb_credentials.png" width="600" alt="RTDB Credentials" />

## 3.6 Calendar Integration Workflow

1. Once the shared calendar is configured and connected with the Firebase backend, the [`scheduler-fetchCalendarEvents`](../functions/functions/scheduler.js) will be running at a certain time interval to fetch all the events on the calendar after the current timestamp.

2. The function will parse the event details from the calendar invite including the coach email, parent uid, and session information for creating meeting records in RTDB under the `/meeting` data node.

<img src="./imgs/calendar/calendar_invite.png" width="600" alt="Calendar Invite" />

3. When the meeting record is added to the RTDB, the [`meeting-onMeetingAdded`](../functions/functions/meeting.js) function will be triggered to generate the anonymous session joining link for the parent and update the calendar invite with the link. We will then trigger the event notification via Google Calendar to the parent & coach so that the initial notification received by the parent will already contain the meeting link.

4. [`scheduler-topsScheduler`](../functions/functions/scheduler.js) runs every minute to check whether there are any meetings that are about to start. If the meeting is going to start in an hour, we will then call the [`opentokApp-opentokApp`](../functions/functions/opentokApp.js) to create the Vonage session for the meeting and update the meeting status to be `scheduled`.

5. When the calendar invite is updated in the shared calendar (e.g. change session start time, duration, etc.), the meeting details for the session in the RTDB will get synchronized in the next `fetchCalendarEvents` execution (only if the session has not been scheduled yet).

[<p align="right">Next: SendGrid Integration</p>](./5_QuickStart_SendGrid.md)
