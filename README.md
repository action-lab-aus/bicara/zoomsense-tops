# Bicara Core

This repository contains the core Bicara infrastructure components, including:

- A Vue.js application that acts as the Bicara coach dashboard and allows parents to easily join a TOPS session and view their learning assets curated by the session coaches
- A list of core Firebase Cloud Functions that act as the serverless backend for the core Bicara infrastructure functionalities
- Integration with [Vonage Video API](https://www.vonage.com.au/communications-apis/video/) to build custom video experience with WebRTC
- SendGrid integration to support email notifications
- Shared Google calendar integration to support TOPS session scheduling and notifications

Bicara leverages the [ZoomSense Plugin Infrastructure](https://gitlab.com/action-lab-aus/zoomsense/zoomsense-plugin-directory) to allow configuring TOPS session templates easily via a list of available plugins which exposes a Vue.js library and/or a number of plugin-specific Firebase functions. These plugins handle the core Bicara capabilities which are not provided by the core Bicara infrastructure and allow a meeting host to turn on/off the plugins needed for a specific TOPS session easily. The full Bicara plugin list can be found in the [`bicara-plugin-directory`](https://gitlab.com/action-lab-aus/bicara/bicara-plugin-directory) repository.

# Quick Start Guide

View the [Quick Start Guide](./docs/1_QuickStart_Instructions.md) to get an understanding of how to launch and run your own Bicara infrastructure.

If you already have a Bicara infrastructure running, jump directly to [Step 5.3](./docs/6_QuickStart_Web_Client.md) to start setting up your environment variables for the Bicara web client & [Step 6.2](./docs/7_QuickStart_Functions.md) for building and deploying the Bicara core functions.

## Bicara Plugin Directory

[Bicara Plugin Directory](https://gitlab.com/action-lab-aus/bicara/bicara-plugin-directory) handles the deployment of plugins needed for the infrastructure (Vue libraries and plugin functions). Detailed instructions on how to build and deploy plugins into the Bicara infrastructure can be found in the directory documentation.
