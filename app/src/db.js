import * as firebase from "firebase/app";
import "firebase/database";
import "firebase/auth";
import "firebase/storage";
import "firebase/functions";

const firebaseConfig = {
  apiKey: process.env.VUE_APP_FB_KEY,
  authDomain: process.env.VUE_APP_FB_AUTH,
  databaseURL: process.env.VUE_APP_RTDB,
  projectId: process.env.VUE_APP_FB_ID,
  storageBucket: process.env.VUE_APP_FB_BUCKET,
  functions_url: process.env.VUE_APP_FB_FUNCTIONS,
  messagingSenderId: process.env.VUE_APP_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_APP_ID,
};

firebase.initializeApp(firebaseConfig);

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsubscribe = firebase.auth().onAuthStateChanged(async (user) => {
      if (user) {
        unsubscribe();
        resolve(user);
      } else resolve(null);
    }, reject);
  });
};

if (process.env.VUE_APP_USE_EMULATOR)
  firebase.functions().useFunctionsEmulator("http://localhost:5001");

export const config = firebaseConfig;
export const db = firebase.database();
export const auth = firebase.auth();
export const storage = firebase.storage();
export const functions = firebase.functions();
export const functionUrl = firebaseConfig.functions_url;
export const Timestamp = firebase.database.ServerValue.TIMESTAMP;
export const googleProvider = firebase.auth.GoogleAuthProvider;
export const createParentProfileFn = functions.httpsCallable(
  "parentAuth-createParentProfile"
);
export const getUserProfileFn = functions.httpsCallable("pip-getUserProfile");
export const getRecordingForSession = functions.httpsCallable(
  "s3-getRecordingForSession"
);
export const getUserModuleDataFn = functions.httpsCallable(
  "pip-getUserModuleData"
);

export const getAssetsForClient = functions.httpsCallable(
  "assets-getAssetsForClient"
);

export const getRecordingForAsset = functions.httpsCallable(
  "s3-getRecordingForAsset"
);

export const PERSISTANCE = firebase.auth.Auth.Persistence;
