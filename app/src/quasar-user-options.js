import "./styles/quasar.sass";
import "@quasar/extras/material-icons/material-icons.css";
import "@quasar/extras/material-icons-outlined/material-icons-outlined.css";
import { Meta, Notify, Loading, Dialog } from "quasar";

export default {
  config: {},
  plugins: {
    Meta,
    Notify,
    Loading,
    Dialog,
  },
};
