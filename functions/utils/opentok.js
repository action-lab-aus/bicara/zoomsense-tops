const functions = require("firebase-functions");
const axios = require("axios");
const admin = require("firebase-admin");
const db = admin.database();
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const localHost = false;
const FUNCTIONS_URL = localHost
  ? environment.localhost_functions_url
  : environment.functions_url;

/**
 * Create Opentok sessions by calling `GET /opentokApp-opentokApp/session/:uid/:key` endpoint,
 * which will create session id & token and save them into the /meetings/uid/key data node
 *
 * The function will be also store a reference data node as /sessionId/uid/meetingid since the
 * only identifiable information from the archive is the sessionId and we need the reference to
 * map the archive files with the meeting
 *
 * @param {*} uid Firebase user id for TOPS coaches
 * @param {*} key TOPS session meeting key (unique event identifier without @google.com)
 * @returns { sessionId, token }
 */
async function createOpentokSessions(uid, key) {
  try {
    // Create an OpenTok session
    const res = await axios.get(
      `${FUNCTIONS_URL}opentokApp-opentokApp/session/${uid}/${key}`
    );

    // Record the cross-reference from session id to a TOPS meeting
    await db
      .ref("sessionRef")
      .child(res.data.sessionId)
      .set({ coachUid: uid, meetingId: key });

    return res.data;
  } catch (error) {
    functions.logger.error(error);
  }
}

exports.createOpentokSessions = createOpentokSessions;
