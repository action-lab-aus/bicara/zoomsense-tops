const admin = require("firebase-admin");
const crypto = require("crypto");
const db = admin.database();
const { totp } = require("otplib");
//adjust the timeout on the token to 1h
totp.options = { step: 60 * 60 };

async function getTotpToken(email) {
  const uid = (await admin.auth().getUserByEmail(email)).uid;
  let secret = (await db.ref(`/firebase-otp/${uid}`).once("value")).val();
  if (!secret) {
    secret = crypto.randomBytes(20).toString("hex");
    await db.ref(`/firebase-otp/${uid}`).set(secret);
  }
  const token = totp.generate(secret);
  return token;
}

exports.getTotpToken = getTotpToken;
