const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(environment.sendgrid.api_key);
const SENDGRID_SENDER = environment.sendgrid.sender;
const SENDGRID_NAME = environment.sendgrid.name;

/**
 * Send SendGrid notification emails with optional attachments using Dynamic Templates
 *
 * @param email Target email address
 * @param dynamicId SendGrid dynamic template id
 * @param params SendGrid Handle Bar parameters (e.g. sessionId, coachEmail, curationUrl)
 * @param attachments An array of email attachmenmts
 *
 * @returns SendGrid emailing client response
 */
async function sendSendGridEmail(email, dynamicId, params, attachments) {
  functions.logger.info("params: ", params);

  let msg = {
    to: email,
    from: {
      name: SENDGRID_NAME,
      email: SENDGRID_SENDER,
    },
    templateId: dynamicId,
    dynamic_template_data: params,
  };

  if (attachments) msg.attachments = attachments;

  const response = await sgMail.send(msg);
  functions.logger.info(response[0].statusCode);
  return response;
}

exports.sendSendGridEmail = sendSendGridEmail;
