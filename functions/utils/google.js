const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const { google } = require("googleapis");
const CALENDAR_ID = environment.google.calendar_id;

async function getOAuthClient() {
  // Get refresh token from RTDB
  const snapshot = await db.ref("credentials").once("value");
  const refreshToken = snapshot.val().refresh_token;

  // Initialize Google OAuth client
  const oauth2Client = new google.auth.OAuth2(
    environment.google.client_id,
    environment.google.client_secret,
    environment.google.redirect_uri
  );

  // Get access token with refresh token & reset the token in Firestore
  oauth2Client.credentials.refresh_token = refreshToken;
  const response = await oauth2Client.getAccessToken();
  const credentials = response.res.data;
  oauth2Client.setCredentials(credentials);
  await db.ref("credentials").update(credentials);
  return oauth2Client;
}

async function updateEventLink(eventId, sessionLink, uid, coachLink) {
  // Add OAuth to to Google client
  const oauth2Client = await getOAuthClient();
  const calendar = google.calendar("v3");
  google.options({ auth: oauth2Client });

  // Fetch the calendar event by event id
  const res = await calendar.events.get({
    calendarId: CALENDAR_ID,
    eventId: eventId,
  });

  let meeting = (
    await db.ref(`meetings/${uid}/${eventId}`).once("value")
  ).val();

  let template = (
    await db.ref(`templates/${meeting.template}`).once("value")
  ).val();

  // Update event description with the anonymous meeting link
  let event = res.data;
  event.summary = `Session ${template.session || ""}: ${template.name}`;
  event.description = `Client - Click here to join Your session: ${sessionLink}\n\nCoach - Visit ${coachLink} to start the session.`;
  await calendar.events.update({
    calendarId: CALENDAR_ID,
    sendUpdates: "none",
    sendNotifications: false,
    eventId: eventId,
    requestBody: event,
  });
}

exports.getOAuthClient = getOAuthClient;
exports.updateEventLink = updateEventLink;
