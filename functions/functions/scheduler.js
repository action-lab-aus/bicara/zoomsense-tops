const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const CALENDER_ADDRESS = environment.calender.address;
const admin = require("firebase-admin");
const db = admin.database();
const ical = require("node-ical");
const _ = require("lodash");
const { getMsDiff } = require("../utils/timeformatter");
const { createOpentokSessions } = require("../utils/opentok");
// const { mapKeys } = require("lodash");
const meetingRef = db.ref("meetings");
const moment = require("moment-timezone");
// const { template } = require("lodash");

// Fetch calendar events from a private .ics link and create Vonage meetinsg in RTDB
// exports.fetchCalendarEvents = functions.https.onRequest(
//   async (request, response) => {
exports.fetchCalendarEvents = functions.pubsub
  .schedule("every 3 minutes")
  .onRun(async (context) => {
    // console.log("Running...");

    // Fetching templates for mapping session names
    const templateSnapshot = await db.ref("templates").once("value");
    const templates = templateSnapshot.val();

    // Load the calendar events from the private .ics link
    const events = await ical.async.fromURL(CALENDER_ADDRESS);

    // console.log(events);

    // Fetch the valid coaches in RTDB
    const coachSnapshot = await db.ref("users").get();
    const coachList = {};
    const coachEmailList = [];

    // console.log(coachSnapshot.val());

    Object.keys(coachSnapshot.val()).forEach((key) => {
      coachEmailList.push(coachSnapshot.val()[key].email);
      coachList[coachSnapshot.val()[key].email] = key;
    });

    // Loop through the calendar events and parse the events details
    const newMeetings = {};
    const promises = [];
    const schedulingRefs = [];
    for (const e of Object.values(events)) {
      try {
        // Skip if it is a past meeting
        if (e.start <= new Date()) continue;

        const startTime = e.start.toISOString();
        const endTime = e.end.toISOString();

        // Parse the session index
        let sessionIdx;
        // let title = e.summary.match(/Session ([0-9])+/);
        // // console.log(title);

        // if (title) {
        //   sessionIdx = title[1];
        // }

        // console.log(sessionIdx);

        // Parse the coach email & patient uid
        let coachEmail;
        let patientUid;
        let parentEmail;
        let template_name = "_default";
        if (
          _.includes(e.description, "Coach: ") &&
          _.includes(e.description, "Client UID: ") &&
          _.includes(e.description, "Template: ")
        ) {
          // console.log(e.attendee);
          const coachArr = e.description.match(
            /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/gi
          );
          const patientArr = e.description.match(/Client UID: (\w*)/);

          const tempArr = e.description.match(/Template: (.*)/);

          // console.log(coachArr);
          // console.log(patientArr);

          if (patientArr.length > 0) patientUid = patientArr[1];
          if (coachArr.length > 0) coachEmail = coachArr[0];
          if (tempArr.length > 0) template_name = tempArr[1];

          console.log(`coach email: ${coachEmail}`);
          console.log(`patient id: ${patientUid}`);
          console.log(`Template: ${template_name}`);

          // Create or update the session details in RTDB

          // console.log(e.attendee);

          if (coachEmailList.indexOf(coachEmail) >= 0) {
            e.attendee.forEach((attendee) => {
              // console.log(attendee.params);
              if (attendee.params.CN !== coachEmail)
                parentEmail = attendee.params.CN;
            });

            console.log(`parentEmail: ${parentEmail}`);

            const coachUid = coachList[coachEmail];
            if (!newMeetings[coachUid]) newMeetings[coachUid] = {};
            const meetingObj = {
              type: "Vonage",
              startTime,
              endTime,
              template: `${template_name}`,
              patientUid: patientUid ? patientUid : "",
              parentEmail,
            };

            const meetingId = e.uid.replace("@google.com", "");

            // console.log(sessionIdx);

            // console.log(template_name);
            // console.log(templates[template_name]);

            // if (sessionIdx) {
            meetingObj.topic = templates[template_name].name;

            newMeetings[coachUid][meetingId] = meetingObj;

            schedulingRefs.push({ coachUid, meetingId, meetingObj });

            promises.push(
              db.ref(`meetings/${coachUid}/${meetingId}`).once("value")
            );
            // }
          } else {
            //not got enough info to schedule:
            functions.logger.info(`No session parsed for ${meetingId}`);
          }
        }
      } catch (ex) {
        functions.logger.error(`failed to schedule ${e.uid} for ${ex}`);
      }
    }

    // functions.logger.info(newMeetings);
    const snapshots = await Promise.all(promises);
    const schedulePromises = [];

    // console.log(snapshots.length);
    //FOR EACH OF THE NEW MEETINGS - INSERT IF ITS NOT THERE, OR UPDATE WITH A LINK IF IT IS (AND THE LINK HAS ONLY JUST BEEN GENERATED)
    for (let i = 0; i < snapshots.length; i++) {
      const { coachUid, meetingId, meetingObj } = schedulingRefs[i];
      //if the meeting does not exist in the db:
      if (!snapshots[i].exists())
        functions.logger.info(`Creating ${meetingId}`);
      schedulePromises.push(
        db.ref(`meetings/${coachUid}/${meetingId}`).set(meetingObj)
      );
      // else {
      //if the meeting exists in the db, then update with new information
      // const { scheduled, sessionLink } = snapshots[i].val();
      // if (!scheduled) {
      //   functions.logger.info(`Updating ${meetingId} with ${meetingObj}`);
      //   db.ref(`meetings/${coachUid}/${meetingId}`).set({
      //     ...meetingObj,
      //     sessionLink,
      //   });
      // }
      // }
    }

    await Promise.all(schedulePromises);

    //*** now check if any of the events have changed: ***
    let allMeetings = (await db.ref("meetings").once("value")).val();
    // console.log(allMeetings);

    const mapEvents = _.mapKeys(events, (v, k) => k.replace("@google.com", ""));
    // console.log(mapEvents);

    //for each coach:
    for (const c of Object.keys(allMeetings)) {
      //for each meeting:
      for (const m of Object.keys(allMeetings[c])) {
        try {
          const meeting = allMeetings[c][m];
          const event = mapEvents[m];

          //if its not there:
          if (!event) {
            functions.logger.info(`Removing ${m} from Bicara`);
            meeting.removedAt = moment().valueOf();
            await db.ref(`removed/${c}/${m}`).set(meeting);
            await db.ref(`meetings/${c}/${m}`).remove();
          } else {
            //if its in the past, ignore
            if (moment(event.start).isBefore(moment())) continue;

            if (!moment(meeting.startTime).isSame(moment(event.start))) {
              console.log(`update ${meeting.topic} with ${event.start}`);
              await db.ref(`meetings/${c}/${m}`).update({
                startTime: event.start.toISOString(),
                endTime: event.end.toISOString(),
              });
            }
          }

          // console.log(meeting.topic);
        } catch (ex) {
          functions.logger.info(`failed to update ${ex}`);
        }
      }
    }
  });

// TOPS meeting scheduler functions executed every minute to find all meetings with type `TOPS`
// - Check whether the meeting will start within in an hour
// - Create an OpenTok session for the meeting
// - Update the the meeting with session details
// - Update Google Calendar with the meeting join link
// exports.topsScheduler = functions.https.onRequest(async (request, response) => {
exports.topsScheduler = functions.pubsub
  .schedule("every 1 minutes")
  .timeZone("Australia/Melbourne")
  .onRun(async (context) => {
    try {
      // Fetch all coaches from RTDB
      const coachSnapshot = await db.ref("users").get();
      const coachUidList = Object.keys(coachSnapshot.val());

      // Retrieve all meetings need to be scheduled
      const meetingSnap = await meetingRef.once("value");
      const meetings = meetingSnap.val();
      const scheduleMeetings = await getTopsMeetings(meetings, coachUidList);
      functions.logger.info("scheduleMeetings: ", scheduleMeetings);

      // Perform scheduling for meetings returned
      const promises = [];
      scheduleMeetings.forEach((meeting) => {
        promises.push(createOpentokSessions(meeting.uid, meeting.key));
      });
      await Promise.all(promises);

      // response.send({ scheduleMeetings });
    } catch (error) {
      functions.logger.error(error);
      // response.send({ error });
    }
  });

/**
 * Get TOPS meetings which need to be scheduled
 * @param {*} meetings Meeting snapshot in Firebase
 * @param {*} coachUidList TOPS coach list
 */
async function getTopsMeetings(meetings, coachUidList) {
  if (!meetings) return [];

  const uidKeys = Object.keys(meetings);
  const promises = [];
  const scheduleMeetings = [];

  // Loop through all uids under the meeting node
  uidKeys.forEach(async (uid) => {
    // Skip regular Zoom meetings
    // if (uid.startsWith("zoom:")) return;

    const uidMeetings = meetings[uid];
    const meetingKeys = Object.keys(uidMeetings);

    // If the uid is not in the coach uid list, skip scheduling
    if (!coachUidList.includes(uid)) {
      meetingKeys.forEach((meetingKey) => {
        if (uidMeetings[meetingKey].type === "Vonage")
          promises.push(
            meetingRef.child(`${uid}/${meetingKey}`).update({
              scheduled: false,
              error: "Invalid User",
            })
          );
      });
    }
    // Otherwise, calculate the time differences between the current time and schedule time
    // If the meeting will start within an hour, then add to the scheduling meeting list
    else {
      meetingKeys.forEach((meetingKey) => {
        let meeting = uidMeetings[meetingKey];
        meeting.uid = uid;
        meeting.key = meetingKey;
        const { scheduled, startTime, endTime, type } = meeting;

        if (type === "Vonage") {
          if (startTime && endTime) {
            const diffMs = getMsDiff(startTime);
            // Schedule meeting that will start in an hour
            if (scheduled === undefined && diffMs <= 60 * 60 * 1000) {
              functions.logger.info(
                `Create Vonage Session ${meetingKey} at ${startTime}`
              );
              promises.push(
                meetingRef
                  .child(`${uid}/${meetingKey}`)
                  .update({ scheduled: true })
              );
              scheduleMeetings.push(meeting);
            }
          } else
            promises.push(
              meetingRef.child(`${uid}/${meetingKey}`).update({
                error: "Missing startTime or endTime of the meeting",
              })
            );
        }
      });
    }
  });

  await Promise.all(promises);
  return scheduleMeetings;
}
