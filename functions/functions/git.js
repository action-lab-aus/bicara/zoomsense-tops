const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();
const path = require("path");
const os = require("os");
const simpleGit = require("simple-git");
const project = "action-lab-aus/bicara/tops-content";
const localPath = path.join(os.tmpdir(), "bicara-core");
const { includes } = require("lodash");
const fs = require("fs");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const rimraf = require("rimraf");
const moment = require("moment");

const token = environment.git.token;
const username = environment.git.username;
const email = environment.git.email;

async function dumpAsFiles() {
  let templates = (await db.ref("templates").once("value")).val();
  let files = [];

  Object.keys(templates).forEach((element) => {
    // if (includes(names, element)) {
    //write content to a file with this name:
    if (element !== "_default") {
      fs.writeFileSync(
        `${localPath}/templates/${element}.json`,
        JSON.stringify(templates[element], null, 2)
      );
      console.log(`${localPath}/templates/${element}.json`);
      files.push(element);
    }
    // console.log(JSON.stringify(templates[element]));
    // }
  });
  return files;
}

function deleteDir(localPath) {
  return new Promise((resolve) => {
    rimraf(localPath, () => {
      resolve();
    });
  });
}

exports.getVersions = functions.https.onCall(async (data, context) => {
  const git = simpleGit();

  // await git.addConfig("user.name", username);

  // await git.addConfig("user.email", email);

  await deleteDir(localPath);

  await git.mirror(
    `https://${username}:${token}@gitlab.com/${project}.git`,
    localPath
  );

  await git.cwd(localPath);

  let versions = await git.tags(["-n"]);
  let tags = [];
  versions.all.forEach((t) => {
    let tag = t.split("   ");
    tags.push({
      when: tag[0],
      what: tag[1].replace(/"/g, ""),
    });
  });

  // console.log(tags);

  return tags;
});

exports.getVersion = functions.https.onCall(async (data, context) => {
  let tag = data.tag || "";
  try {
    await deleteDir(localPath);

    const git = simpleGit();

    // await git.addConfig("user.name", username);

    // await git.addConfig("user.email", email);

    await git.clone(
      `https://${username}:${token}@gitlab.com/${project}.git`,
      localPath
    );

    await git.cwd(localPath);

    //checkout this version:
    if (tag.length > 0) await git.checkout(`tags/${tag}`);

    //read each file and return:

    let files = fs.readdirSync(`${localPath}/templates`);

    let output = {};
    files.forEach((element) => {
      let json = JSON.parse(
        fs.readFileSync(path.join(localPath, "templates", element))
      );
      output[element.replace(".json", "")] = json;
    });

    return output;
  } catch (e) {
    throw new functions.https.HttpsError("invalid-argument", e.message);
  }
});

exports.saveVersion = functions.https.onCall(async (data, context) => {
  try {
    // console.log(context.auth);
    // let claims = await context.auth.getIdTokenResult();
    // console.log(claims);

    if (context.auth.token.coach || context.auth.token.admin) {
      //clone it
      await deleteDir(localPath);

      const git = simpleGit();

      await git.clone(
        `https://${username}:${token}@gitlab.com/${project}.git`,
        localPath
      );

      //write the files
      let filelist = await dumpAsFiles();

      console.log(filelist);

      await git.cwd(localPath);

      //for some reason need this to run before anything will work past this point...
      await git.status();

      await git.addConfig("user.name", username);

      await git.addConfig("user.email", email);

      filelist.forEach(async (element) => {
        await git.add(`templates/${element}.json`);
      });

      // console.log(await git.status());

      // //push it with a tag

      await git.commit("new version added");

      await git.tag([
        "-a",
        `${moment().valueOf()}`,
        "-m",
        `"${data.message || "new version"}"`,
      ]);

      // //https://gitlab.com/action-lab-aus/bicara/tops-content
      // //https://gitlab.com/action-lab-aus/bicara/tops-content.git
      // // await git.clone(repoUrl, localPath);
      await git.push(`https://${username}:${token}@gitlab.com/${project}.git`);
      await git.pushTags(
        `https://${username}:${token}@gitlab.com/${project}.git`
      );
      return { message: "done" };
    } else
      throw new functions.https.HttpsError("invalid-argument", "Not an admin");
  } catch (e) {
    throw new functions.https.HttpsError("invalid-argument", e.message);
  }
});
