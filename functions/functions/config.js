const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();
const { mapValues, keyBy, compact } = require("lodash");
const humanInterval = require("human-interval");

function convertScheduleToCurrent(plugins, global) {
  plugins = compact(plugins);
  global = compact(global);

  let currentState = {
    plugins: mapValues(
      keyBy(plugins, (o) => o.plugin.toLowerCase()),
      "settings"
    ),
  };

  let currentGlobal = {
    plugins: mapValues(
      keyBy(global, (o) => o.plugin.toLowerCase()),
      "settings"
    ),
  };

  for (const key in currentState.plugins) {
    if (!currentState.plugins[key]) currentState.plugins[key] = {};
    currentState.plugins[key].enabled = true;
  }

  for (const key in currentGlobal.plugins) {
    if (!currentGlobal.plugins[key]) currentGlobal.plugins[key] = {};
    currentGlobal.plugins[key].enabled = true;
    currentGlobal.plugins[key].global = true;
  }

  return { ...currentState.plugins, ...currentGlobal.plugins };
}

exports.cleanDemoConfig = functions.pubsub
  .schedule("every day 04:00")
  .onRun(async (context) => {
    const keys = Object.keys((await db.ref("/config").once("value")).val());
    // console.log(keys);

    let promises = [];

    for (const id of keys) {
      if (id.startsWith("demo_")) {
        promises.push(db.ref(`config/${id}`).remove());
      }
    }

    await Promise.allSettled(promises);

    functions.logger.info(`removed ${promises.length} demo configs`);
  });

exports.onCurrentSectionUpdate = functions.database
  .ref("/config/{meetingId}/current/currentSection")
  .onWrite(async (snapshot, context) => {
    const meetingId = context.params.meetingId;
    const currentSnap = await db.ref(`config/${meetingId}`).once("value");
    // console.log(currentSnap);
    const { current, schedule } = currentSnap.val();
    // console.log(schedule);
    try {
      const plugins =
        schedule.schedule.script[parseInt(current.currentSection)].plugins;
      const global = schedule.schedule.global;

      //...schedule.global,

      await db
        .ref(`config/${meetingId}/current/currentState/plugins`)
        .set(convertScheduleToCurrent(plugins, global));

      // Set time when updated
      const timestamp = Date.now();
      await db.ref(`config/${meetingId}/current/currentState`).update({
        startedAt: timestamp,
        sectionLoaded: parseInt(current.currentSection),
      });

      // Record currentSection change metadata under the `/data` node
      await db
        .ref(`/data/sectionChange/${meetingId}/_history/${timestamp}`)
        .set({ _currentSection: current.currentSection });
    } catch (e) {
      functions.logger.error(`Out of range ${current.currentSection}`, e);
    }
  });

// Triggers the advance logic on a current section based on a plugin trigger (e.g. video coming to end):
exports.onPluginTriggerAdvance = functions.https.onCall(
  async (data, context) => {
    try {
      //get meeting id:
      let meetingId = data.meetingId;
      functions.logger.info(`${meetingId}`);
      const currentSnap = await db.ref(`config/${meetingId}`).once("value");
      const { current, schedule } = currentSnap.val();
      const currentState = schedule.schedule.script[current.currentSection];

      //if advance of current section is auto, then automatically advance.
      if (currentState.advance.type === "auto") {
        let newval = current.currentSection + 1;
        //update current section:
        await db.ref(`config/${meetingId}/current/currentSection`).set(newval);
      }

      //if advance of current section if manual, don't do anything.
      //if advance of the current section is scheduled, then dont do anything.
    } catch (e) {
      functions.logger.error(`Trigger fail`, e);
    }
  }
);

exports.processTimedAdvances =
  // functions.https.onRequest(
  // async (request, response) => {
  functions.pubsub.schedule("every 1 minutes").onRun(async () => {
    try {
      //get meeting id:
      const meetings = (await db.ref(`config`).once("value")).val();
      //for each meeting in config:

      for (let meetingId of Object.keys(meetings)) {
        try {
          if (!meetingId.startsWith("demo_")) {
            // functions.logger.info(`${meetingId}`);
            //grab current advance node:
            const current = meetings[meetingId].current;
            const schedule = meetings[meetingId].schedule.schedule;

            // const { current, schedule } = currentSnap.val();
            const currentState = schedule.script[current.currentSection];

            //if advance of current section is auto, then automatically advance.
            if (currentState.advance.type === "timed") {
              let interval = humanInterval(currentState.advance.when);
              let starttime = current.currentState.startedAt;
              //grab timing info, and calculate if its true

              //since the last script segment:
              if (Date.now() >= starttime + interval) {
                //advance if it is
                let newval = current.currentSection + 1;
                //update current section:
                await db
                  .ref(`config/${meetingId}/current/currentSection`)
                  .set(newval);
              }
            }
          }
        } catch (e) {
          functions.logger.info(`${meetingId}`, e);
        }
      }

      //if advance of current section if manual, don't do anything.
      //if advance of the current section is scheduled, then dont do anything.
    } catch (e) {
      functions.logger.error(`Timed advance fail`, e);
    }

    return null;
  });

// Firebase onUpdate function triggered when the meeting template updates to refresh
// the `currentState` of the session
exports.onSessionTemplateUpdate = functions.database
  .ref("/config/{meetingId}/schedule")
  .onUpdate(async (change, context) => {
    try {
      // console.log("change: ", change);
      const meetingId = context.params.meetingId;
      const currentSnap = await db.ref(`config/${meetingId}`).once("value");
      const { current, schedule } = currentSnap.val();

      const plugins =
        schedule.schedule.script[parseInt(current.currentSection)].plugins;
      const global = schedule.schedule.global;

      // Update the current schedule
      await db
        .ref(`config/${meetingId}/current/currentState/plugins`)
        .set(convertScheduleToCurrent(plugins, global));

      // Set time when updated
      await db
        .ref(`config/${meetingId}/current/currentState/startedAt`)
        .set(Date.now());
    } catch (e) {
      functions.logger.error("onSessionTemplateUpdate Error: ", e);
    }
  });
