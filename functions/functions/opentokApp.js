const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const API_KEY = environment.opentok.api_key;
const API_SECRET = environment.opentok.api_secret;
const admin = require("firebase-admin");
const db = admin.database();

const express = require("express");
const cors = require("cors");
const path = require("path");
const OpenTok = require("opentok");
const opentok = new OpenTok(API_KEY, API_SECRET);

const app = express();
app.use(cors({ origin: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, "public")));

app.get("/session/:uid/:key", async (req, res) => {
  try {
    opentok.createSession(
      { mediaMode: "routed" },
      async function (err, session) {
        if (err) return res.send(err);

        const expireTime = new Date().getTime() / 1000 + 2 * 60 * 60;
        const token = session.generateToken({
          role: "publisher",
          expireTime,
        });

        // Save the session id for the current user
        await db.ref(`meetings/${req.params.uid}/${req.params.key}`).update({
          session: session.sessionId,
          token,
        });

        return res.send({
          sessionId: session.sessionId,
          token,
        });
      }
    );
  } catch (error) {
    return res.send(error.message);
  }
});

app.get("/archive/stop/:archiveId", async (req, res) => {
  try {
    opentok.stopArchive(req.params.archiveId, function (err, archive) {
      if (err)
        return res
          .status(500)
          .send(
            "Could not stop archive " + archiveId + ". error=" + err.message
          );
      res.json(archive);
    });
  } catch (error) {
    res.send(error.message);
  }
});

app.post("/archive/start/:sessionId", async (req, res) => {
  try {
    const sessionId = req.params.sessionId;
    functions.logger.info("sessionId: ", sessionId);

    let archiveOptions = {
      name: sessionId,
      hasAudio: true,
      hasVideo: true,
      outputMode: "composed",
    };

    opentok.startArchive(
      sessionId,
      archiveOptions,
      async function (err, archive) {
        if (err) {
          functions.logger.error(err);
          return res
            .status(500)
            .send(
              "Could not start archive for session " +
                sessionId +
                ". error=" +
                err.message
            );
        }

        // Save the archive id for the current session
        await db
          .ref(`data/recording/${req.body.meetingId}/archive`)
          .set(archive.id);

        return res.json(archive);
      }
    );
  } catch (error) {
    functions.logger.error(error);
    res.send(error.message);
  }
});

exports.opentokApp = functions.https.onRequest(app);
