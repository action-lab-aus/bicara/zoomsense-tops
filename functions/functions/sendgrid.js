const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const { sendSendGridEmail } = require("../utils/sendEmail");
const CURATION_READY = environment.sendgrid.curation_ready;
const CURATION_ERROR = environment.sendgrid.curation_error;
const SESSION_REMINDER = environment.sendgrid.session_reminder;
const SITE_URL = environment.site_url;
const { getTotpToken } = require("../utils/totp");
const moment = require("moment-timezone");

// Check the reminder queue and send reminders to the session participants
// exports.sendReminders = functions.https.onRequest(async (request, response) => {
exports.sendReminders = functions.pubsub
  .schedule("every 10 minutes")
  .onRun(async (context) => {
    //add on 5 hours from now (in the future)
    const currentTime = new Date().getTime() + 5 * 60 * 60 * 1000;
    const snapshot = await db
      .ref("reminders")
      .orderByChild("when")
      .endAt(currentTime)
      .once("value");

    const deletePromises = [];
    const sendgridPromises = [];

    if (!snapshot.val()) return;

    // console.log(snapshot.val());

    for (const key of Object.keys(snapshot.val())) {
      // return;
      // console.log(key);
      const data = snapshot.val()[key];
      // functions.logger.info('data: ', data);

      let meeting = (
        await db.ref(`meetings/${data.coachUid}/${key}`).once("value")
      ).val();

      // console.log(meeting);
      if (meeting) {
        const mom = moment(meeting.startTime);

        if (meeting.sessionLink) {
          // console.log(`${data.offset} days`);
          sendgridPromises.push(
            db.ref(`emails`).push({
              email: data.patientEmail,
              template: SESSION_REMINDER,
              data: {
                parentName: data.patientName,
                sessionName: data.topic,
                reminderDay: `${data.offset} ${
                  data.offset <= 1 ? "day" : "days"
                }`,
                link: meeting.sessionLink,
                sessionStartTime: mom
                  .tz("Australia/Melbourne")
                  .format("h:mm a"),
                sessionStartDate: mom
                  .tz("Australia/Melbourne")
                  .format("Do MMMM"),
              },
            })
          );

          deletePromises.push(db.ref(`reminders/${key}`).remove());
        }
      }
    }

    await Promise.all(deletePromises);
    await Promise.all(sendgridPromises);
  });

exports.onEmailNeedsSending = functions.database
  .ref("/emails/{emailid}")
  .onCreate(async (snapshot, context) => {
    console.log(`Sending: ${context.params.emailid}`);
    // console.log(snapshot.after.val());
    try {
      const { id, template, email, data, requiresAuth } = snapshot.val();
      // console.log(id, template, email, data, requiresAuth);

      //if requires that an auth link be generated:
      if (requiresAuth) {
        const token = await getTotpToken(email);
        data.token = token;
        data.parentLink = `${
          environment.site_url
        }/#/myjourney?token=${token}&email=${encodeURIComponent(email)}`;
      }

      sendSendGridEmail(email, template, data, []);

      //delete from node:
      db.ref(`emails/${context.params.emailid}`).remove();
    } catch (e) {
      functions.logger.error(e);
    }
  });

exports.onTranscriptionStatusChange = functions.database
  .ref("/data/transcription/{meetingId}/status")
  .onCreate(async (snapshot, context) => {
    try {
      const sessionId = context.params.meetingId;
      const status = snapshot.val();

      // Get the coach uid from the `/data/meetingMeta` node
      const meta = (
        await db.ref(`data/meetingMeta/${sessionId}`).once("value")
      ).val();
      const coachUid = meta.uid;

      // Get the coach email from the users node
      const coachSnapshot = await db.ref(`users/${coachUid}`).once("value");
      const coachEmail = coachSnapshot.val().email;

      // Trigger an email notification when the archive is successfully transcribed
      if (status === "transcribed") {
        functions.logger.info("coachEmail: ", coachEmail);
        functions.logger.info("sessionId: ", sessionId);
        functions.logger.info(
          "curationUrl: ",
          `${SITE_URL}/dashboard/${coachUid}/${sessionId}`
        );

        await sendSendGridEmail(coachEmail, CURATION_READY, {
          sessionId,
          coachEmail,
          curationUrl: `${SITE_URL}/#/dashboard/${coachUid}/${sessionId}`,
        });
      }

      // Trigger an email notification when an error occurred during transcription
      if (status === "error") {
        functions.logger.info("coachEmail: ", coachEmail);
        functions.logger.info("sessionId: ", sessionId);

        await sendSendGridEmail(coachEmail, CURATION_ERROR, {
          sessionId,
          coachEmail,
        });
      }
    } catch (error) {
      functions.logger.error(error);
    }
  });
