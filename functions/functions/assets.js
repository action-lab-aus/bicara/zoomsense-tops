const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const { find, map, uniq, flatten, filter } = require("lodash");
const cfsign = require("aws-cloudfront-sign");
const moment = require("moment-timezone");

const freg = /::: (.*):(.*)/g;

/**
 * String.prototype.replaceAll() polyfill
 * https://gomakethings.com/how-to-replace-a-section-of-a-string-with-another-one-with-vanilla-js/
 * @author Chris Ferdinandi
 * @license MIT
 */
if (!String.prototype.replaceAll) {
  String.prototype.replaceAll = function (str, newStr) {
    // If a regex pattern
    if (
      Object.prototype.toString.call(str).toLowerCase() === "[object regexp]"
    ) {
      return this.replace(str, newStr);
    }

    // If a string
    return this.replace(new RegExp(str, "g"), newStr);
  };
}

async function getFields(meetingid, clientid, output) {
  let obj = (await db.ref(`config/${meetingid}/schedule`).once("value")).val();

  // console.log(obj);

  if (!obj) return;

  let text = obj.raw;

  if (!text) {
    // console.log("returning");
    return;
  }

  let meetingname = obj.name;

  const match = [...text.matchAll(freg)];
  let result = [];
  // console.log(text);
  for (let r of match) {
    // console.log(r[1]);
    const address = r[1].startsWith("s-")
      ? `sequence/slides/${clientid}/current/${r[1].replace("s-", "")}`
      : `data/slides/${meetingid}/current/${r[1]}`;
    result.push({
      name: r[2].trim().replaceAll("\\n", "").replaceAll('"', ""),
      type: r[1].startsWith("s-") ? "sequence" : "data",
      field: r[1].replace("s-", ""),
      address: address,
    });
  }

  if (!output[meetingid]) output[meetingid] = {};

  output[meetingid].fields = result;
  output[meetingid].name = meetingname;

  // console.log(meetingname);
}

//gets the checkin data (question, answer) based on the responses for that meeting (if there are any)
async function getCheckin(meetingid, clientid, checkindata, outputs) {
  let output = [];
  if (checkindata && checkindata[meetingid]) {
    output = checkindata[meetingid];
    // for (const entry in checkindata[meetingid]) {

    // output.push({}
    // }
  }

  // console.log(output);

  if (!outputs[meetingid]) outputs[meetingid] = {};

  outputs[meetingid].checkin = output;
  // return output;
}

async function getMeta(meetingid, clientid, output)
{
  let obj = (await db.ref(`meetings/${clientid}/${meetingid}`).once("value")).val();

  console.log(obj);

  if (!obj) return;

  if (!output[meetingid]) output[meetingid] = {};

  output[meetingid].session = obj;
}

exports.getPrepInfoForClient = functions.https.onCall(async (data, context) => {
  try {
    if (context.auth.token.coach || context.auth.token.admin) {
      let clientid = data.clientid;

      //get meetings:
      const meetings_db = (
        await db.ref(`patients/${clientid}`).once("value")
      ).val();
      // console.log(meetings_db);
      if (meetings_db === null) return {};

      let meetings = Object.keys(meetings_db.meetings);

      // console.log(meetings_db);

      let checkins = (
        await db.ref(`sequence/topsCheckin/${clientid}`).once("value")
      ).val();

      // console.log("checkins", checkins);

      // let checkinmeta = (await db.ref(`meta/checkin`).once("value")).val();

      // console.log(meetings);

      let output = {};
      let promises = [];
      for (const meeting of meetings) {
        promises.push(getFields(meeting, clientid, output));
        promises.push(getMeta(meeting, meetings_db.coach, output));
        getCheckin(meeting, clientid, checkins, output);
        // promises.push(getMeetingInfo(meeting, output));
      }

      await Promise.all(promises);

      return output;
    } else
      throw new functions.https.HttpsError("invalid-argument", "Not an admin");
  } catch (e) {
    throw new functions.https.HttpsError("invalid-argument", e.message);
  }
});

//gets all the assets for a client:
exports.getAssetsForClient = functions.https.onCall(async (data, context) => {
  // console.log(context.auth);
  let email = data.email;

  if (
    context.auth.token.coach ||
    context.auth.token.admin ||
    context.auth.token.email === email
  ) {
    // Fetch the current patient from their email (i.e. not the temp user??)
    const patients = (await db.ref("patients").once("value")).val();
    let tmpPatient;
    Object.keys(patients).forEach((key) => {
      if (patients[key].email === email)
        tmpPatient = { ...patients[key], id: key };
    });
    let client = tmpPatient;

    if (!tmpPatient) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Missing client"
      );
    }

    // console.log(tmpPatient);

    //get all assets for these meetings:
    const promises = [];
    Object.keys(tmpPatient.meetings || {}).forEach((key) => {
      promises.push(db.ref(`data/curationAsset/${key}`).once("value"));
    });

    const curatedMeetingAssets = await Promise.all(promises);

    const labels = (await db.ref(`meta/tags`).once("value")).val();

    const sequence = (
      await db.ref(`sequence/slides/${client.id}`).once("value")
    ).val();

    const allAssets = [];
    for (const item of curatedMeetingAssets) {
      if (item.val()) {
        for (const itemKey of Object.keys(item.val())) {
          // Object.keys(item.val()).forEach(async (itemKey) => {
          const data = item.val()[itemKey];
          // console.log(data);
          if (
            itemKey !== "remaining" &&
            data.status === "generated" &&
            data.toParents
          ) {
            //get transcript:
            let fulltranscript = (
              await db.ref(`data/transcription/${item.key}/raw`).once("value")
            ).val();

            let start = timeCodeToSecs(data.startTimecode);
            let end = timeCodeToSecs(data.endTimecode);

            let trans = [];
            for (const t of fulltranscript) {
              // console.log(t);
              let s = srcCodeToSecs(t.startTime);
              let e = srcCodeToSecs(t.endTime);
              if ((s >= start && s <= end) || (e >= start && e <= end))
                trans.push(t);
            }

            let meetingMeta = (
              await db.ref(`data/meetingMeta/${item.key}`).once("value")
            ).val();

            let slideAn = (
              await db.ref(`data/slides/${item.key}`).once("value")
            ).val();

            // console.log(meetingMeta);

            //get fields:
            let fields = await getSlideDataForSection(
              getRealTime(meetingMeta, start),
              getRealTime(meetingMeta, end),
              sequence
            );

            let annotations = await getSlideAnnotationForSection(
              getRealTime(meetingMeta, start),
              getRealTime(meetingMeta, end),
              slideAn
            );

            //get labels:
            let labs = [];
            for (const l of data.labels || []) {
              // console.log(l);
              let lab = find(labels, { name: l });
              labs.push(lab);
            }

            const signingParams = {
              keypairId: environment.aws.cloudfront_key,
              privateKeyString: environment.aws.cloudfront_private_key,
              expireTime: moment().add(1, "days").unix() * 1000,
            };

            var signedUrl = cfsign.getSignedUrl(
              `${environment.aws.cloudfront_url}/${data.s3UrlCurated.replace(
                ".mp4",
                ".mp3"
              )}`,
              signingParams
            );

            allAssets.push({
              ...data,
              signedUrl,
              transcript: trans,
              labels: labs,
              id: itemKey,
              fields,
              annotations,
              sessionIdx: tmpPatient.meetings[item.key],
              patientUid: tmpPatient.id,
              patientName: tmpPatient.name,
              meetingId: item.key,
            });
          }
        }
      }
    }
    return {
      client: { ...client, data: sequence ? sequence.current : {} },
      assets: allAssets,
    };
  } else {
    throw new functions.https.HttpsError("invalid-argument", "Access denied");
  }
});

function timeCodeToSecs(time) {
  let tt = time.split(":");

  return (
    parseInt(tt[0]) * 3600 +
    parseInt(tt[1]) * 60 +
    parseInt(tt[2]) +
    parseInt(tt[3]) / 100
  );
}

//convert a timestamp of the asset (relative to start of meeting) into a real-world time.
function getRealTime(meetingMeta, time) {
  return meetingMeta.actualStartTime + time * 1000;
}

function srcCodeToSecs(time) {
  let tt = time.replace(",", ".").split(":");

  return parseInt(tt[0]) * 3600 + parseInt(tt[1]) * 60 + parseFloat(tt[2]);
}

async function getSlideAnnotationForSection(start, end, slides) {
  const resultArr = [];

  // console.log(`start: ${start}, end:${end}`);
  let allHistory = slides["_history"];
  // console.log(Object.entries(allHistory));
  allHistory = Object.fromEntries(
    Object.entries(allHistory).filter(
      (history) => history[0] >= start && history[0] <= end
    )
  );

  let fields = uniq(flatten(map(allHistory, (a) => Object.keys(a))));

  for (let field of fields) {
    if (field.startsWith("_annotation")) {
      resultArr.push({
        id: field,
        value: slides.current[field],
      });
    }
  }

  // console.log(fields);

  return resultArr;
}

async function getSlideDataForSection(start, end, sequence) {
  //get slide change timestamp data for this meeting:
  // const slides = (await db.ref(`data/slides/${meetingId}`).once("value")).val();

  //slide content:
  // const schedule = (
  //   await db.ref(`config/${meetingId}/schedule/schedule`).once("value")
  // ).val();

  const resultArr = [];

  //get a list of fields that were edited in this time frame:
  const fieldMap = getFieldsEdited(sequence, start, end);
  // console.log(fieldMap);

  //for each field, go get the value:
  for (let field of fieldMap) {
    resultArr.push({
      id: field,
      value: sequence.current[field],
    });
  }

  //get list of slides that were moved through in this time-segment:
  // const slideMap = getSlideTimestamps(slides, start, end);

  return resultArr;
}

function getFieldsEdited(sequence, start, end) {
  // console.log(`start: ${start}, end:${end}`);

  let allHistory = sequence["_history"];
  allHistory = Object.fromEntries(
    Object.entries(allHistory).filter(
      (history) => history[0] >= start && history[0] <= end
    )
  );

  return uniq(flatten(map(allHistory, (a) => Object.keys(a))));
}

function getSlideTimestamps(slides, start, end) {
  // console.log(`start: ${start}, end:${end}`);
  let allHistory = slides["_history"];
  allHistory = Object.fromEntries(
    Object.entries(allHistory).filter(
      (history) =>
        history[1]["_slide"] !== undefined &&
        history[0] >= start &&
        history[0] <= end
    )
  );

  return uniq(map(allHistory, "_slide"));
}
