const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const axios = require("axios");

// Get user profile data from PiP+
exports.getUserProfile = functions.https.onCall(async (data, context) => {
  const isCoach = context.auth.token.coach;

  if (!context.auth || !context.auth.uid || !isCoach)
    throw new functions.https.HttpsError(
      "failed-precondition",
      "The function must be called while authenticated with a coach custom claim."
    );

  if (!data.uid)
    throw new functions.https.HttpsError(
      "invalid-argument",
      'The function must be called with one argument "uid".'
    );

  try {
    const headers = {};
    headers[environment.pip.api_key] = environment.pip.api_value;
    const response = await axios.get(
      `${environment.pip.api_endpoint}/user/${data.uid}`,
      { headers }
    );

    return response.data;
  } catch (error) {
    functions.logger.error(error);
    return error;
  }
});

// Get user module completion data from PiP+
exports.getUserModuleData = functions.https.onCall(async (data, context) => {
  const isCoach = context.auth.token.coach;

  if (!context.auth || !context.auth.uid || !isCoach)
    throw new functions.https.HttpsError(
      "failed-precondition",
      "The function must be called while authenticated with a coach custom claim."
    );

  if (!data.uid || !data.moduleId)
    throw new functions.https.HttpsError(
      "invalid-argument",
      'The function must be called with two arguments "uid" and "moduleId".'
    );

  try {
    const headers = {};
    headers[environment.pip.api_key] = environment.pip.api_value;
    const response = await axios.get(
      `${environment.pip.api_endpoint}/user/${data.uid}/module/${data.moduleId}`,
      { headers }
    );

    return response.data;
  } catch (error) {
    functions.logger.error(error);
    return error;
  }
});
