const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const crypto = require("crypto");
const { totp } = require("otplib");
totp.options = { step: 60 * 60 };
const cors = require("cors")({
  origin: "*",
});
totp.options = { step: 300 };
const { sendSendGridEmail } = require("../utils/sendEmail");
const { getTotpToken } = require("../utils/totp");
const TOTP_TOKEN = environment.sendgrid.totp_token;

exports.createParentProfile = functions.https.onCall(async (data, context) => {
  const isCoach = context.auth.token.coach;
  const { pipUid, name, email, coach } = data;

  functions.logger.info("pipUid: ", pipUid);
  functions.logger.info("name: ", name);
  functions.logger.info("email: ", email);
  functions.logger.info("coach: ", coach);

  if (!isCoach) return { status: 401, message: `Unauthorized` };
  if (!pipUid)
    return {
      status: 403,
      message: `Missing the 'pipUid' field for the request...`,
    };
  if (!name)
    return {
      status: 403,
      message: `Missing the 'name' field for the request...`,
    };
  if (!email)
    return {
      status: 403,
      message: `Missing the 'email' field for the request...`,
    };
  if (!coach)
    return {
      status: 403,
      message: `Missing the 'coach' field for the request...`,
    };

  try {
    await db.ref("parentWhitelist").push(email);
    const user = await admin.auth().createUser({
      uid: pipUid,
      email,
      displayName: name,
      password: crypto.randomBytes(20).toString("hex"),
      disabled: false,
    });
    await db.ref(`patients/${user.uid}`).set({ name, email, coach });

    return {
      status: 200,
      message: `Successfully created an account for ${name} with email ${email}`,
    };
  } catch (error) {
    functions.logger.error(error.message);
    return {
      status: 500,
      message: error.message,
    };
  }
});

exports.parentGetTotpToken = functions.https.onRequest((request, response) =>
  cors(request, response, async () => {
    const email = request.body.email;

    try {
      const token = await getTotpToken(email);

      await sendSendGridEmail(email, TOTP_TOKEN, {
        token,
        url: `${environment.site_url}/#/myjourney?token=${token}&email=${encodeURIComponent(email)}`,
      });

      response.status(201).json({
        success: true,
        message: `A link to access your journey has been sent to ${email}!`,
        token,
      });
    } catch (error) {
      functions.logger.error(error.message);
      response.status(202).json({
        success: false,
        message: error.message,
      });
    }
  })
);

// authenticates the 6 diget code and passes back a custom login token that the firebase then uses on the client to auth the user.
exports.parentAuthenticate = functions.https.onRequest((request, response) =>
  cors(request, response, async () => {
    try {
      const email = request.body.email;
      const token = request.body.token;
      const uid = (await admin.auth().getUserByEmail(email)).uid;
      const secretValue = (
        await db.ref(`/firebase-otp/${uid}`).once("value")
      ).val();
      const result = totp.verify({ token, secret: secretValue });

      if (!result)
        throw new Error("This link is invalid, it may have timed out.");

      const customToken = await admin.auth().createCustomToken(uid);

      response.status(200).json({
        success: true,
        message: `${email} successfully authenticated!`,
        customToken,
      });
    } catch (error) {
      functions.logger.error(error.message);
      response.status(202).json({
        success: false,
        message: error.message,
      });
    }
  })
);
