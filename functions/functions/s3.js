const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const cfsign = require("aws-cloudfront-sign");
const moment = require("moment-timezone");

exports.getRecordingForSession = functions.https.onCall(
  async (data, context) => {
    try {
      // console.log(data);
      if (data.meetingid && data.coachid) {
        let meeting = (
          await db
            .ref(`/meetings/${data.coachid}/${data.meetingid}`)
            .once("value")
        ).val();

        if (meeting === null)
          throw new functions.https.HttpsError(
            "invalid-argument",
            "Invalid meeting"
          );

        if (typeof context.auth === "undefined")
          throw new functions.https.HttpsError(
            "invalid-argument",
            "Not logged in"
          );

        if (
          meeting.patientUid === context.auth.uid ||
          data.coachid === context.auth.uid ||
          context.auth.token.admin
        ) {
          //go and get the s3 url:
          let recording = (
            await db
              .ref(`/data/recording/${data.meetingid}/s3UrlVideo`)
              .once("value")
          ).val();

          const signingParams = {
            keypairId: environment.aws.cloudfront_key,
            privateKeyString: environment.aws.cloudfront_private_key,
            expireTime: moment().add(1, "days").unix() * 1000,
          };

          var signedUrl = cfsign.getSignedUrl(
            `${environment.aws.cloudfront_url}/${recording}`,
            signingParams
          );

          return signedUrl;
        } else {
          throw new functions.https.HttpsError(
            "invalid-argument",
            "No permissions"
          );
        }
      } else {
        throw new functions.https.HttpsError(
          "invalid-argument",
          "Missing {meetingid} or {coachid}"
        );
      }
    } catch (e) {
      throw new functions.https.HttpsError("invalid-argument", e.message);
    }
  }
);

exports.getRecordingForAsset = functions.https.onCall(async (data, context) => {
  try {
    // console.log(data);
    if (data.meetingid && data.assetid) {
      // let meeting = (
      //   await db
      //     .ref(`/meetings/${data.coachid}/${data.meetingid}`)
      //     .once("value")
      // ).val();

      // if (meeting === null)
      //   throw new functions.https.HttpsError(
      //     "invalid-argument",
      //     "Invalid meeting"
      //   );

      if (typeof context.auth === "undefined")
        throw new functions.https.HttpsError(
          "invalid-argument",
          "Not logged in"
        );

      if (context.auth.token.coach || context.auth.token.admin) {
        //go and get the s3 url:
        let recording = (
          await db
            .ref(
              `/data/curationAsset/${data.meetingid}/${data.assetid}/s3UrlCurated`
            )
            .once("value")
        ).val();

        const signingParams = {
          keypairId: environment.aws.cloudfront_key,
          privateKeyString: environment.aws.cloudfront_private_key,
          expireTime: moment().add(1, "days").unix() * 1000,
        };

        var signedUrl = cfsign.getSignedUrl(
          `${environment.aws.cloudfront_url}/${recording.replace(
            ".mp4",
            ".mp3"
          )}`,
          signingParams
        );

        return signedUrl;
      } else {
        throw new functions.https.HttpsError(
          "invalid-argument",
          "No permissions"
        );
      }
    } else {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Missing {meetingid} or {coachid}"
      );
    }
  } catch (e) {
    throw new functions.https.HttpsError("invalid-argument", e.message);
  }
});
