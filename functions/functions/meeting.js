const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const admin = require("firebase-admin");
const db = admin.database();
const { updateEventLink } = require("../utils/google");
const { getMeetingKey } = require("./anonymous");
const SITE_URL = environment.site_url;
const crypto = require("crypto");

exports.onMeetingAdded = functions.database
  .ref("/meetings/{uid}/{meetingId}")
  .onCreate(async (snapshot, context) => {
    // Grab the current value of what was written to the Realtime Database.
    const meeting = snapshot.val();
    const { uid, meetingId } = context.params;
    functions.logger.log("meetingId: ", meetingId);
    const template = meeting.template;
    functions.logger.log("template: ", template);

    // Check whether the parent record exists under the `/patients` node
    const patientRecord = (
      await db.ref(`patients/${meeting.patientUid}`).once("value")
    ).val();

    //create patient record if this patient does not exist:
    if (!patientRecord) {
      const parentEmail = meeting.parentEmail;
      await db.ref("parentWhitelist").push(parentEmail);
      const user = await admin.auth().createUser({
        uid: meeting.patientUid,
        email: parentEmail,
        displayName: parentEmail,
        password: crypto.randomBytes(20).toString("hex"),
        disabled: false,
      });
      await db
        .ref(`patients/${user.uid}`)
        .set({ name: parentEmail, email: parentEmail, coach: uid });
    }

    functions.logger.log("patientid: ", meeting.patientUid);

    // Generate anonymous meeting link for patients to join
    const anonymousToken = await getMeetingKey(
      meetingId,
      uid,
      meeting.patientUid
    );
    const anonymousLink = `${SITE_URL}/#/meeting?token=${anonymousToken}`;
    await db.ref(`meetings/${uid}/${meetingId}`).update({
      sessionLink: anonymousLink,
    });

    const coachLink = `${SITE_URL}/#/dashboard/${meeting.patientUid}/${meetingId}`;

    // TOPS meeting only
    if (meeting.type === "Vonage") {
      // Record the meeting under the patient's data node (for access rule checking)
      await db
        .ref(`patients/${meeting.patientUid}/meetings/${meetingId}`)
        .set(meeting.template);
      // Update the event description with the meeting link
      await updateEventLink(meetingId, anonymousLink, uid, coachLink);
    }

    // Default
    let sessionTemplate = {
      schedule: {
        global: {},
        script: [
          {
            advance: { type: "manual" },
            name: "Default Meeting",
            plugins: [],
          },
        ],
      },
    };

    if (template) {
      const templateSnap = await db.ref(`templates/${template}`).once("value");
      sessionTemplate = templateSnap.val();
    }

    await db.ref(`config/${meetingId}`).set({
      current: {
        currentSection: 0,
      },
      schedule: sessionTemplate,
    });

    // Push the section names to `/data/meetingMeta`
    // const sections = [];
    // sessionTemplate.schedule.script.forEach((section) => {
    //   sections.push(section.name);
    // });
    // await db.ref(`/data/meetingMeta/${meetingId}/sections`).set(sections);
  });

/**
 * When `actualEndTime` is added to the meeting node, push the data to the /data/meetingMeta node
 *
 * @param {String} uid Host ID
 * @param {String} meetingId Session ID
 */
exports.onMeetingEnded = functions.database
  .ref("/meetings/{uid}/{meetingId}/actualEndTime")
  .onCreate(async (snapshot, context) => {
    try {
      const actualEndTime = snapshot.val();
      const { meetingId } = context.params;

      await db.ref(`/data/meetingMeta/${meetingId}`).update({ actualEndTime });
    } catch (error) {
      functions.logger.error("onMeetingEnded Error: ", error);
    }
  });
