const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();
const {
  mapValues,
  filter,
  intersection,
  keys,
  mapKeys,
  map,
  groupBy,
} = require("lodash");
const e = require("express");

// exports.onTemplateUpdate = functions.database
//   .ref("/templates")
//   .onWrite(async (change, context) => {
//     //list of titles:
//     let templates = (await db.ref("/templates").once("value")).val();

//     let grouped = mapKeys(groupBy(templates, "program"), (obj, key) => {
//       return key === "undefined" ? "default" : key;
//     });

//     for (const k in grouped) {
//       grouped[k] = map(grouped[k], "name");
//     }

//     await db.ref(`meta/session/`).set(grouped);
//   });

exports.loadTemplates = functions.https.onRequest(async (request, response) => {
  await db.ref("templates").set(templates);
  response.send("Successfully loaded templates...");
});

exports.listTemplates = functions.https.onCall(async (data, context) => {
  let templates = (
    await db.ref("templates").orderByChild("name").once("value")
  ).val();

  let mykeys = keys(templates);
  templates = mapValues(mykeys, (x) => {
    const n = templates[x];
    return {
      name: n.name,
      tags: n.tags,
      id: x,
      program: n.program,
      session: n.session,
    };
  });

  if (data.tags) {
    templates = filter(templates, (x) => {
      return intersection(x.tags, data.tags).length > 0;
    });
  }
  return templates;
});

const templates = {
  session_1: {
    name: "TOPS Session 1",
    tags: ["tops"],
    schedule: {
      global: [
        {
          plugin: "Recording",
        },
      ],
      script: [
        {
          name: "Introduction",
          advance: { type: "timed", when: "+5min" },
          plugins: [
            {
              plugin: "StaticConfig",
              settings: {
                muteall: true,
                selfunmute: false,
                spotlight: "host",
              },
            },
            {
              plugin: "VideoPlayer",
              settings: {
                src: "myVideo.mp4",
                loop: true,
                audio: false,
              },
            },
            {
              plugin: "BackgroundAudio",
              settings: {
                src: "myAudio.mp3",
                level: 0.8,
                loop: true,
              },
            },
          ],
        },

        {
          name: "Main Class",
          advance: { type: "manual" },
          plugins: [
            {
              plugin: "BackgroundAudio",
              settings: {
                src: "myAudio1.mp3",
                level: 0.1,
                loop: true,
              },
            },
            {
              plugin: "UIConfig",
            },
            {
              plugin: "Ticker",
              settings: {
                config: "bottom",
              },
            },
            {
              plugin: "LiveIndicator",
              settings: {
                config: "topRight",
              },
            },
            {
              plugin: "Titles",
              settings: {
                config: "Connect to zoom chat",
                titles: ["Title 1", "Title 2", "Title 3"],
              },
            },
          ],
        },

        {
          name: "Outro",
          advance: { type: "auto", when: "5mins" },
          plugins: [
            {
              plugin: "VideoPlayer",
              settings: {
                src: "myVideo.mp4",
                loop: true,
                audio: false,
              },
            },
            {
              plugin: "BackgroundAudio",
              settings: {
                src: "myAudio.mp3",
                level: 0.8,
                loop: true,
              },
            },
          ],
        },
      ],
    },
  },
};
