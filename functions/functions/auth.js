const functions = require("firebase-functions");
const admin = require("firebase-admin");
const db = admin.database();
const auth = admin.auth();

exports.checkWhitelistUser = functions.auth.user().onCreate(async (user) => {
  try {
    functions.logger.info(`Is Anon: ${user.email === null}`);
    if (user.email === null) return;

    const topsWhitelist = (await db.ref("topsWhitelist").once("value")).val();

    // If the authenticated user is not in the whitelist
    if (Object.values(topsWhitelist).indexOf(user.email) < 0) {
      const parentWhitelist = (
        await db.ref("parentWhitelist").once("value")
      ).val();

      // Remove the user account if it does not exist in both parentWhitelist & topsWhitelist
      if (Object.values(parentWhitelist).indexOf(user.email) < 0) {
        // Delete the new user account
        await auth.deleteUser(user.uid);

        // Push a status flag under /userVerification to notify the frontend of the verification result
        await db.ref("users").child(user.uid).set({ verified: false });
      }
    } else {
      // Set the custom claim to coach for the authenticated user
      await admin.auth().setCustomUserClaims(user.uid, { coach: true });
      await db
        .ref("users")
        .child(user.uid)
        .set({ verified: true, email: user.email });
    }
  } catch (error) {
    functions.logger.error(error);
  }
});
