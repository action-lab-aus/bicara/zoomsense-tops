const functions = require("firebase-functions");
const environment = functions.config()[process.env.GCLOUD_PROJECT];
const encryptionKey = environment.anonymous.encryption_key;
const encryptionSecret = environment.anonymous.encryption_secret;
const Cryptr = require("cryptr");
const crypto = require("crypto");
const cryptr = new Cryptr(encryptionSecret);
const admin = require("firebase-admin");
const db = admin.database();
const anonymousTokens = db.ref("anonymous/tokens");

// Generate anonymous signin link with encrypted tokens for co-hosts
exports.generateLink = functions.https.onCall(async (data, context) => {
  if (!context.auth || !context.auth.uid)
    throw new functions.https.HttpsError(
      "failed-precondition",
      "The function must be called while authenticated."
    );

  const meetingid = data.meetingid;
  const hostuid = data.hostuid;

  if (!meetingid || !hostuid)
    throw new functions.https.HttpsError(
      "invalid-argument",
      'The function must be called with two arguments "meetingid" and "hostuid".'
    );

  return this.getAnonymousToken(meetingid, hostuid);
});

// Decrypt and validate a token passed from the browser. Sets up all the right meta-data in the rtdb for the anonymous session.
exports.decryptToken = functions.https.onCall(async (data, context) => {
  if (!context.auth || !context.auth.uid)
    throw new functions.https.HttpsError(
      "failed-precondition",
      "The function must be called while authenticated."
    );

  if (!data.token)
    throw new functions.https.HttpsError(
      "invalid-argument",
      'The function must be called with one argument "token" containing the anonymous login details.'
    );

  // Get the token passed from the client
  const token = (
    await db.ref(`anonymous/keys/${data.token}`).once("value")
  ).val().token;

  try {
    const decryptedString = cryptr.decrypt(token);
    const decryptedArr = decryptedString.split("|");
    if (decryptedArr[0] === encryptionKey) {
      const meetingid = decryptedArr[1];
      const hostuid = decryptedArr[2];
      const patientUid = decryptedArr[3];

      const tokenSnap = await anonymousTokens
        .child(`${hostuid}/${meetingid}/token`)
        .once("value");
      if (tokenSnap.val() === token) {
        await db
          .ref(`/anonymous/users/${context.auth.uid}/hosts/${hostuid}`)
          .set({ createdAt: new Date().getTime() });
        await db
          .ref(`/anonymous/users/${context.auth.uid}/meetings/${meetingid}`)
          .set({ createdAt: new Date().getTime() });
        await db
          .ref(`/anonymous/users/${context.auth.uid}/sequences/${patientUid}`)
          .set({ createdAt: new Date().getTime() });

        return { meetingid: meetingid, hostuid: hostuid };
      } else
        throw new functions.https.HttpsError(
          "invalid-token",
          "The token provided has expired."
        );
    } else
      throw new functions.https.HttpsError(
        "invalid-argument",
        "The token provided is not a valid token containing the anonymous login details."
      );
  } catch (error) {
    throw new functions.https.HttpsError(
      "invalid-argument",
      "The token provided is not a valid token containing the anonymous login details."
    );
  }
});

exports.getAnonymousToken = async (meetingid, hostuid, patientUid) => {
  let encryptStr = `${encryptionKey}|${meetingid}|${hostuid}`;
  if (patientUid) encryptStr += `|${patientUid}`;
  const encryptedString = cryptr.encrypt(encryptStr);

  await anonymousTokens
    .child(`${hostuid}/${meetingid}`)
    .set({ token: encryptedString });

  return encryptedString;
};

exports.getMeetingKey = async (meetingid, hostuid, patientUid) => {
  const encryptedString = await this.getAnonymousToken(
    meetingid,
    hostuid,
    patientUid
  );

  let meetingKey;
  let duplicateKey = true;
  while (duplicateKey) {
    meetingKey = crypto.randomBytes(24).toString("hex");
    const snapshot = await db.ref(`anonymous/keys/${meetingKey}`).once("value");
    if (!snapshot.exists()) duplicateKey = false;
  }

  await db.ref(`anonymous/keys/${meetingKey}`).set({ token: encryptedString });

  return meetingKey;
};
